# Multiplexer

## Code to analyse data taken from multiplexer box.
1. Edit the config file to your parameters
2. Run ``` python Multiplexer.py 0 ``` for help
3. Run ``` python Multiplexer.py 1 ``` for Vbd
4. Run ``` python Multiplexer.py 2 ``` for Rq
5. Run ``` python Multiplexer.py 3 ``` for DCR
6. Run ``` python Multiplexer.py 4 channel_number ``` for single channel analysis

## Complemetary code.
1. ```batch_analysis.py``` and ```analysis_Mari.c``` are codes for summary plots of correlated noise, RQ and VBD

python-numpy is needed: ``` sudo apt-get install python-numpy ```
