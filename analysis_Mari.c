#include <stdio.h>
#include <stdlib.h>
#include "TCanvas.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2D.h"
#include "TFile.h"
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include "TGraph.h"
#include "THStack.h"
#include "TStyle.h"

int choose_color(int num){
    const int num_10 = num/10;
    int color_grade = num%10;
    int color_base = 0;

    switch(num_10){
        case (0):
            color_base = kPink;
            break;
        case (1):
            color_base = kOrange;
            break;
        case (2):
            color_base = kTeal;
            break;
        case (3):
            color_base = kSpring;
            break;
        case (4):
            color_base = kAzure;
            break;
        case (5):
            color_base = kViolet;
            break;
    }

    return color_base + color_grade;
}
void analysis_Mari(){

    std::string datapath_mux = "/home/analysis/Mux_data/";
    std::string detector_type = "H2017";
    std::string fullpath_mux = datapath_mux + detector_type + "/" + detector_type + "_";

    int my_det[] = {5,24,49,55,56,78,79,110,111,112,113,114,115,116,117,118,126,145,167,186,194,208,227,249,268,295,324,325,326,338,339,340,341,342,370,389,407,409,410,433,434,435,436,445,457,458,491,502,471,484};
    const std::vector<int> detectors_mux ( my_det, my_det + sizeof(my_det) / sizeof(my_det[0]));
    
    std::string vbd_name = "VBD_box_2.root";
    std::string rq_name = "RQ_box_2.root";

    std::string histo_title = "V_{BD} and R_{Q} correlation for 50 detectors";
    //TH2D* htempcorr= new TH2D(histo_title.c_str(),"Vbd-Rq correlation;V_{BD} [V];R_{Q} [#Omega]",10000,385.,510,10000,51.6,53.8);


    TCanvas *ccorrfinal = new TCanvas("Vbd-Rq correlation", "Vbd-Rq correlation", 800, 600);
    std::vector<TH2D*> hist_mux;
    // = new TH2D("asd","asd",10000,385.,510,10000,51.6,53.8);
    THStack *stack = new THStack();
    
    for(int i=0; i<detectors_mux.size(); i++){
        std::stringstream det;
        det << "Detector "<< detectors_mux[i];
        std::cout << det.str() << std::endl;
        TH2D* hist= new TH2D(det.str().c_str(), "V_{BD} and R_{Q} correlation for 50 detectors; RQ[#Omega] ; Vbd [V]", 1000,385.,510,1000,51.6,53.8);
        hist_mux.push_back(hist);
    }

    for(int i=0; i<detectors_mux.size(); i++){
        std::stringstream fullpath_vbd_ss;
        fullpath_vbd_ss << fullpath_mux << detectors_mux[i] << "/" << vbd_name;

        std::stringstream fullpath_rq_ss;
        fullpath_rq_ss  << fullpath_mux << detectors_mux[i] << "_RQ/" << rq_name;

        TFile * fvbd = new TFile(fullpath_vbd_ss.str().c_str(), "r");
        std::cout << "Analysing file: " << fullpath_vbd_ss.str()<<std::endl;

        TCanvas* cvbd = (TCanvas*)fvbd->Get("c1_n3");
        std::cout<< cvbd<< std::endl;
        
        TGraph* gvbd = (TGraph*)cvbd->GetPrimitive("Graph");

        TFile* frq = new TFile(fullpath_rq_ss.str().c_str(),"r");
        std::cout<< "Analysing file: "<< fullpath_rq_ss.str() << std::endl;
        TCanvas* crq = (TCanvas*)frq->Get("c1");
        TGraph* grq = (TGraph*)crq->GetPrimitive("Graph");

        fvbd->Close();
        frq->Close();
        //std::cout << "************* Init Histo" << std::endl;
        hist_mux[i]->SetDirectory(0);
        //std::cout<<  "************* Filling Histo" << std::endl;
        for(int pt = 0; pt < grq->GetN(); pt++){
            double xv, yv, xr, yr;
            gvbd->GetPoint(pt,xv,yv);
            grq->GetPoint(pt,xr,yr);
            hist_mux[i]->Fill(yr,yv);
        }
        delete grq;
        delete crq;
        delete gvbd;
        delete cvbd;
        //std::cout << "************* Beautyfication" << std::endl;
        hist_mux[i]->SetMarkerStyle(6);
        int color = choose_color(i);

        hist_mux[i]->SetMarkerColor(color);
        //hist_mux[i]->SetName(det.str().c_str());
        //hist_mux[i]->SetTitle(det.str().c_str());
        stack->Add(hist_mux[i]);
        //hist_mux.push_back(hist);
    }
    
    std::cout<< "Creating final canvas" << std::endl;
    //TCanvas *ccorrfinal = new TCanvas("Vbd-Rq correlation", "Vbd-Rq correlation", 800, 600);
    ccorrfinal->cd();
    gStyle->SetOptStat(0);
    std::cout << "Drawing in the Canvas" << std::endl;
//    stack->Draw("nostack hist");
    for(int i=0; i<50; i++){
        //hist_mux[i]->SetDirectory(0);
        if(i==0)hist_mux[i]->Draw("");
        
        else hist_mux[i]->Draw("same");
    }

}


