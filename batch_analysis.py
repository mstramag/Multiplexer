import numpy as np
from array import array
from ROOT import TH1F, TH2F, TCanvas, TGraph, TLegend, TF1, TMath, TMultiGraph, TFile, TTree, TNtuple, TIter, TObjLink, TColor
import ROOT

# detector type
detector_type = "H2017"

#-----------------------------------------------------------------------
# Multiplexer data

# path to data
datapath_mux = "/home/analysis/Mux_data/"
fullpath_mux = datapath_mux + detector_type + "/" + detector_type + "_"

# list of detectors you want to plot together
# QA samples batch1
detectors_mux = [5,24,49,55,56,78,79,110,111,112,113,114,115,116,117,118,126,145,167,186,194,208,227,249,268,295,324,325,326,338,339,340,341,342,370,389,407,409,410,433,434,435,436,445,457,458,491,502,471,484]
#detectors_mux = [5,24,49,55,56,78,79,110,111,112,113,114,115,116,117,118,126,145,167,186,194,208,227,249,268,295,324,325,326,338,339,340,342,370,389,407,409,410,433,434,435,436,445,457,458,491,502,471,484]

# Vbd file name
vbd_name = "VBD_box_2.root"
# Rq  file name
rq_name = "RQ_box_2.root"

tree = TNtuple("Channel specific","Channel specific","vbd:rq")
minmax = TNtuple("Array specific","Array specific","minmaxvbd:minmaxrq")

for det in detectors_mux:
    fullpath_vbd = fullpath_mux + str(det) + "/" + vbd_name
    fullpath_rq  = fullpath_mux + str(det) + "_RQ" + "/" + rq_name    
    # Get Vbd
    fvbd = TFile(fullpath_vbd,"r")
    print("Analyzing file: " + fullpath_vbd)
    
    # Get the canvas
    cvbd = fvbd.Get("c1_n3")
    # Get the graph on the canvas
    gvbd = cvbd.GetPrimitive("Graph")
    
    # Get Rq
    frq = TFile(fullpath_rq,"r")
    print("Analyzing file: " + fullpath_rq)
    
    # Get the canvas
    crq = frq.Get("c1")
    # Get the graph on the canvas
    grq = crq.GetPrimitive("Graph")
    
    Mv = 0
    mv = 1e9
    Mr = 0
    mr = 1e9
    for pt in range(grq.GetN()):
        xv = ROOT.Double(0.0)
        yv = ROOT.Double(0.0)
        gvbd.GetPoint(pt,xv,yv)
        xr = ROOT.Double(0.0)
        yr = ROOT.Double(0.0)
        grq.GetPoint(pt,xr,yr)
        tree.Fill(yv,yr)
        if yv>Mv:
            Mv=yv
        if yv<mv:
            mv=yv
        if yr>Mr:
            Mr=yr
        if yr<mr:
            mr=yr
    minmax.Fill(Mv-mv,Mr-mr)

# Vbd distribution
cv = TCanvas("Vbd","Vbd",800,600)
#vbd_tree.Draw("vbd")
tree.Draw("vbd")
htempv = cv.GetPrimitive("htemp")
htempv.SetName("Breakdown voltage")
htempv.GetXaxis().SetTitle("V_{BD} [V]")
htempv.GetYaxis().SetTitle("Number of entries")
htempv.SetTitle("Breakdown voltage for " + str(len(detectors_mux)) + " detectors")

# Vbd min-max distribution
cvminmax = TCanvas("Vbdminmax","Vbdminmax",800,600)
minmax.Draw("minmaxvbd")
htempv_minmax = cvminmax.GetPrimitive("htemp")
htempv_minmax.SetName("Max-Min Vbd")
htempv_minmax.GetXaxis().SetTitle("V_{BD}^{max}-V_{BD}^{min} [V]")
htempv_minmax.GetYaxis().SetTitle("Number of entries")
htempv_minmax.SetTitle("Max-Min breakdown voltage within one SiPM array for " + str(len(detectors_mux)) + " detectors")

# Rq distribution
cr = TCanvas("Rq","Rq",800,600)
#rq_tree.Draw("rq")
tree.Draw("rq")
htempr = cr.GetPrimitive("htemp")
htempr.SetName("Quenching resistor")
htempr.GetXaxis().SetTitle("R_{Q} [#Omega]")
htempr.GetYaxis().SetTitle("Number of entries")
htempr.SetTitle("Quenching resistor for " + str(len(detectors_mux)) + " detectors")

# Rq min-max distribution
crminmax = TCanvas("Rqminmax","Rqminmax",800,600)
minmax.Draw("minmaxrq")
htempr_minmax = crminmax.GetPrimitive("htemp")
htempr_minmax.SetName("Max-Min Rq")
htempr_minmax.GetXaxis().SetTitle("R_{Q}^{max}-R_{Q}^{min} [#Omega]")
htempr_minmax.GetYaxis().SetTitle("Number of entries")
htempr_minmax.SetTitle("Max-Min quenching resistor within one SiPM array for " + str(len(detectors_mux)) + " detectors")

# Vbd-Rq correlation
ccorr = TCanvas("Vbd-Rq corr","Vbd-Rq corr",800,600)
tree.Draw("vbd:rq")
htempcorr = ccorr.GetPrimitive("htemp")
htempcorr.SetName("Vbd-Rq correlation")
htempcorr.GetXaxis().SetTitle("R_{Q} [#Omega]")
htempcorr.GetYaxis().SetTitle("V_{BD} [V]")
htempcorr.SetMarkerStyle(7)
htempcorr.SetTitle("V_{BD} and R_{Q} correlation for " + str(len(detectors_mux)) + " detectors")
ccorr.SetGrid(1)
# Vbd-Rq min-max correlation
ccorrminmax = TCanvas("Vbd-Rq min-max corr","Vbd-Rq min-max corr",800,600)
minmax.Draw("minmaxvbd:minmaxrq")
htempcorr_minmax = ccorrminmax.GetPrimitive("htemp")
htempcorr_minmax.SetName("Vbd-Rq correlation")
htempcorr_minmax.GetXaxis().SetTitle("R_{Q}^{max}-R_{Q}^{min} [#Omega]")
htempcorr_minmax.GetYaxis().SetTitle("V_{BD}^{max}-V_{BD}^{min} [V]")
htempcorr_minmax.SetMarkerStyle(7)
htempcorr_minmax.SetTitle("V_{BD} and R_{Q} Max-Min correlation for " + str(len(detectors_mux)) + " detectors")
ccorrminmax.SetGrid(1)

#-----------------------------------------------------------------------
# Correlated noise analysis

datapath_noise = "/home/analysis/Analysis_waveforms/"
fullpath_noise = datapath_noise + detector_type + "/" + detector_type + "_"

# list of detectors you want to plot together
#detectors_noise = ["434_ch21","434_ch85","435_ch6","435_ch70","436_ch102","436_ch38","445_ch101","445_ch37","457_ch1","457_ch65","458_ch22","458_ch86","471_ch118","471_ch54","484_ch117","484_ch53","491_ch2","491_ch66","502_ch5","502_ch69","57_ch21","57_ch22"]

detectors_noise = ["110_ch10","110_ch74","111_ch25","111_ch89","112_ch26","112_ch90","113_ch105","113_ch41","114_ch106","114_ch42","115_ch121","115_ch57","116_ch122","116_ch58","117_ch13","117_ch77","118_ch14","118_ch78","126_ch29","126_ch93","145_ch30","145_ch94","167_ch109","167_ch45","186_ch110","186_ch46","194_ch125","194_ch61","208_ch126","208_ch62","227_ch17","227_ch65","24_ch18","24_ch82","434_ch21","434_ch85","435_ch6","435_ch70","436_ch102","436_ch38","445_ch101","445_ch37","457_ch1","457_ch65","458_ch22","458_ch86","471_ch118","471_ch54","484_ch117","484_ch53","491_ch2","491_ch66","49_ch33","49_ch97","502_ch5","502_ch69","55_ch34","55_ch98","56_ch113","56_ch49","57_ch21","57_ch22","58_ch117","58_ch53","5_ch17","5_ch81","78_ch114","78_ch50","79_ch73","79_ch9"]

# list of points for evaluation of the correlated noise
op_pts = [2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0]
corr_noise_types = ["Tot","DirXtalk","AP","DelXtalk"]
corr_noise_values = ["mean","min","detector_min","max","detector_max","err_max","n","rms"]

mean_corr_noise = {op_pt: {corr_noise_type: {key: (ROOT.Double(0) if key!="min" else ROOT.Double(100)) for key in corr_noise_values} for corr_noise_type in corr_noise_types} for op_pt in op_pts}

# distribution of correlated noise at a certain operation point
distr_pt = 3.5
corr_tree = TNtuple("Correlated noise at #DeltaV="+str(distr_pt)+"V","Correlated noise at #DeltaV="+str(distr_pt)+"V","Tot:DirXtalk:DelXtalk:AP")
rebin = 4
max_range = 35

for det in detectors_noise:
    fullpath = fullpath_noise + det + "/" + "noiseanalysis.root"
    
    # Get Vbd
    f = TFile(fullpath,"r")
    print("Analyzing file: " + fullpath)
    
    # Get the canvas
    c = f.Get("Correlated Noise")
    # Get the graphs on the canvas
    obj = ROOT.TObject()
    Next = ROOT.TIter(c.GetListOfPrimitives())
    obj = Next()
    #~ gTot = Next() # total correlated noise graph
    #~ gDirXtalk = Next() # direct cross-talk graph
    #~ gAP = Next() # afterpulse graph
    #~ gDelXtalk = Next() # delayed cross-talk graph
    
    graphs = {}
    for corr_noise_type in corr_noise_types:
        graphs[corr_noise_type] = Next()
    
    for oppt in op_pts:
        
        for corr_noise_type in corr_noise_types:
            
            mean_corr_noise[oppt][corr_noise_type]["mean"] += graphs[corr_noise_type].Eval(oppt)
            mean_corr_noise[oppt][corr_noise_type]["rms"] += graphs[corr_noise_type].Eval(oppt)*graphs[corr_noise_type].Eval(oppt)
            mean_corr_noise[oppt][corr_noise_type]["n"] += 1
            if graphs[corr_noise_type].Eval(oppt) > mean_corr_noise[oppt][corr_noise_type]["max"]:
                mean_corr_noise[oppt][corr_noise_type]["max"] = graphs[corr_noise_type].Eval(oppt)
                mean_corr_noise[oppt][corr_noise_type]["detector_max"] = det
            if graphs[corr_noise_type].Eval(oppt) < mean_corr_noise[oppt][corr_noise_type]["min"]:
                mean_corr_noise[oppt][corr_noise_type]["min"] = graphs[corr_noise_type].Eval(oppt)
                mean_corr_noise[oppt][corr_noise_type]["detector_min"] = det

    
    corr_tree.Fill(graphs["Tot"].Eval(distr_pt), graphs["DirXtalk"].Eval(distr_pt), graphs["DelXtalk"].Eval(distr_pt), graphs["AP"].Eval(distr_pt))

for op_pt in op_pts:
    print("OP {0:.1f}".format(op_pt))
    for corr_noise_type in corr_noise_types:
        mean_corr_noise[op_pt][corr_noise_type]["mean"] = mean_corr_noise[op_pt][corr_noise_type]["mean"]/mean_corr_noise[op_pt][corr_noise_type]["n"]
        mean_corr_noise[op_pt][corr_noise_type]["rms"] = TMath.Sqrt((mean_corr_noise[op_pt][corr_noise_type]["rms"]/mean_corr_noise[op_pt][corr_noise_type]["n"])-(mean_corr_noise[op_pt][corr_noise_type]["mean"]*mean_corr_noise[op_pt][corr_noise_type]["mean"]))
        mean_corr_noise[op_pt][corr_noise_type]["err_max"] = max((mean_corr_noise[op_pt][corr_noise_type]["mean"]-mean_corr_noise[op_pt][corr_noise_type]["min"]),(mean_corr_noise[op_pt][corr_noise_type]["max"]-mean_corr_noise[op_pt][corr_noise_type]["mean"]))
        
        # gives which detector had minimum/maximum correlated noise
        print('\033[91m'+"maximum {} (corr.noise)".format(corr_noise_type)+" of {0:.1f}".format(mean_corr_noise[op_pt][corr_noise_type]["max"])+" at detector {}".format(mean_corr_noise[op_pt][corr_noise_type]["detector_max"])+'\033[0m')
        print('\033[92m'+"minimum {} (corr.noise)".format(corr_noise_type)+" of {0:.1f}".format(mean_corr_noise[op_pt][corr_noise_type]["min"])+" at detector {}".format(mean_corr_noise[op_pt][corr_noise_type]["detector_min"])+'\033[0m')

gMean = {corr_noise_type: ROOT.TGraphAsymmErrors(len(op_pts)) for corr_noise_type in corr_noise_types}

for i,op_pt in enumerate(op_pts):
    for corr_noise_type in corr_noise_types:
        gMean[corr_noise_type].SetPoint(i,op_pt,mean_corr_noise[op_pt][corr_noise_type]["mean"])
        #gMean[corr_noise_type].SetPointError(i,0,mean_corr_noise[op_pt][corr_noise_type]["err_max"])
        #gMean[corr_noise_type].SetPointError(i,0,0,mean_corr_noise[op_pt][corr_noise_type]["mean"]-mean_corr_noise[op_pt][corr_noise_type]["min"],mean_corr_noise[op_pt][corr_noise_type]["max"]-mean_corr_noise[op_pt][corr_noise_type]["mean"])
        gMean[corr_noise_type].SetPointError(i,0,0,mean_corr_noise[op_pt][corr_noise_type]["rms"],mean_corr_noise[op_pt][corr_noise_type]["rms"])

cnoise = TCanvas("MeanCorrNoise","MeanCorrNoise",800,600)
frame = TH2F("frame","frame",100,op_pts[0]-0.5,op_pts[-1]+0.5,int(float(max_range)/0.1),0,max_range)
frame.SetTitle("Correlated noise averaged over " + str(len(detectors_noise)) + " channels")
frame.GetXaxis().SetTitle("#DeltaV [V]")
frame.GetYaxis().SetTitle("Correlated noise [%]")
frame.Draw()

cnoisedistr = TCanvas("CorrelatedNoiseDistr","CorrelatedNoiseDistr",800,600)
framedistr = TH2F("framedistr","framedistr",int(float(corr_tree.GetMaximum("Tot")*1.2)/0.1),0,corr_tree.GetMaximum("Tot")*1.2,150,0,30)
framedistr.SetTitle("Correlated noise at #DeltaV="+str(distr_pt)+"V for " + str(len(detectors_noise)) + " channels")
framedistr.GetXaxis().SetTitle("Correlated noise [%]")
framedistr.GetYaxis().SetTitle("Number of entries")
framedistr.Draw()

leg = ROOT.TLegend(0.11,0.65,0.45,0.89)
leg.SetFillColor(0)
legdistr = ROOT.TLegend(0.55,0.65,0.89,0.89)
legdistr.SetFillColor(0)

for corr_noise_type in corr_noise_types:
    gMean[corr_noise_type].SetMarkerStyle(24)
    
    color = 0
    legent = ""
    if corr_noise_type=="Tot":
        color = 2
        legent = "Total"
    
    if corr_noise_type=="DirXtalk":
        color = 4
        legent = "Direct cross-talk"
    
    if corr_noise_type=="DelXtalk":
        color = ROOT.kGreen+2
        legent = "Delayed cross-talk"
    
    if corr_noise_type=="AP":
        color = ROOT.kOrange+8
        legent = "After-pulse"
    
    cnoise.cd()
    gMean[corr_noise_type].SetMarkerColor(color)
    gMean[corr_noise_type].SetLineColor(color)
    leg.AddEntry(gMean[corr_noise_type],legent,"pl")
    gMean[corr_noise_type].Draw("PL+")
    
    cnoisedistr.cd()
    corr_tree.Draw(corr_noise_type,"","same")
    htempnoise = cnoisedistr.GetPrimitive("htemp")
    htempnoise.SetName("distr"+corr_noise_type)
    htempnoise.SetLineColor(color)
    htempnoise.SetLineWidth(2)
    htempnoise.SetFillColor(color)
    htempnoise.SetFillStyle(3001)
    legdistr.AddEntry(gMean[corr_noise_type],legent,"l")
    htempnoise.Rebin(rebin)

cnoise.cd()
leg.Draw()
frame.SetStats(0)
cnoise.SetGrid(1)

cnoisedistr.cd()
legdistr.Draw()
framedistr.SetStats(0)
cnoisedistr.SetGrid(1)
